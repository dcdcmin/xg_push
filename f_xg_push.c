#include "f_xg_push.h"
#include "f_xg_typedef.h"
#include "xg_push_builder.h"
#include "f_http_client.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_XG_PUSH_SEND_LEN	2048

#define XG_PUSH_SERVER 	"183.61.46.161"
#define XG_PUSH_PORT	80


#define 	XGPUSH_URL  						"/v2/push/single_device"
#define 	XGPUSH_USER_AGENT					"receiver/Receiver/1.1"
#define 	XGPUSH_CONTENT_TYPE 				"application/x-www-form-urlencoded"
#define  	XGPUSH_HOST_NAME					"openapi.xg.qq.com"	

typedef struct _HslXGPush{
	char* server_ip;
	unsigned short port;
}HslXGPush;

static  HslXGPush* X = NULL;

int f_xg_push_init(void){
	if(X != NULL){
		return -1;
	}

	X = (HslXGPush*)malloc(sizeof(HslXGPush));
	return_val_if_fail(X != NULL, -1);

	X->server_ip = strdup(XG_PUSH_SERVER);
	X->port = XG_PUSH_PORT;

	return 0;
}

static int _request(char* param, size_t len){
	FHttpClient* http_client = f_http_client_create(X->server_ip, X->port);
	if(http_client == NULL){
		return -1;
	}

	f_http_client_set_url(http_client, XGPUSH_URL);
	f_http_client_set_host(http_client, XGPUSH_HOST_NAME);
	f_http_client_set_user_agent(http_client, XGPUSH_USER_AGENT);
	f_http_client_set_content_type(http_client, XGPUSH_CONTENT_TYPE);

	int post_ret = -1;
	post_ret = f_http_client_post(http_client, param, len);

	f_http_client_destroy(http_client);

	return post_ret;
}

int f_xg_push_token_android(char* access_id, \
										char* secret_key, \
										char* title, \
										char* content, \
										char* token){
	return_val_if_fail(X != NULL, -1);
	return_val_if_fail(access_id != NULL && secret_key != NULL && title != NULL && content != NULL && token != NULL, -1);

	XGPushBuilder* builder = xg_push_builder_create(access_id, secret_key);
	return_val_if_fail(builder != NULL, -1);

	char param_buff[MAX_XG_PUSH_SEND_LEN] = {0};
	int ret = -1;
	ret = xg_push_builder_build_android_notifation(builder, title, content, token, param_buff, MAX_XG_PUSH_SEND_LEN);

	if(ret < 0){
		printf("Error: build android notifation failed!\n");
		xg_push_builder_destroy(builder);
		return -1;
	}

	xg_push_builder_destroy(builder);
	
	return _request(param_buff, strlen(param_buff));
}

int f_xg_push_token_ios(char* access_id, \
									char* secret_key, \
									char* content, \
									char* token,\
									XGPushEnvironment env){
	return_val_if_fail(X != NULL, -1);
	return_val_if_fail(access_id != NULL && secret_key != NULL && content != NULL && token != NULL, -1);

	XGPushBuilder* builder = xg_push_builder_create(access_id, secret_key);
	return_val_if_fail(builder != NULL, -1);

	char param_buff[MAX_XG_PUSH_SEND_LEN] = {0};
	int ret = -1;
	ret = xg_push_builder_build_ios_notifation(builder, env, content, token, param_buff, MAX_XG_PUSH_SEND_LEN);
	if(ret < 0){
		printf("Error: build android notifation failed!\n");
		xg_push_builder_destroy(builder);
		return -1;
	}
	
	xg_push_builder_destroy(builder);
	return _request(param_buff, strlen(param_buff));
}

