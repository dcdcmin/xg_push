#ifndef __URL_ENCODE_H__
#define __URL_ENCODE_H__

char *url_encode(char const *s, int len, int *new_length);
int url_decode(char *str, int len);


#endif /**/
