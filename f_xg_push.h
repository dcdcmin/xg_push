#ifndef __F_XG_PUSH_H__
#define __F_XG_PUSH_H__

#include "f_xg_typedef.h"

int f_xg_push_init(void);

int f_xg_push_token_android(char* access_id, \
										char* secret_key, \
										char* title, \
										char* content, \
										char* token);

int f_xg_push_token_ios(char* access_id, \
									char* secret_key, \
									char* content, \
									char* token,\
									XGPushEnvironment env);


#endif /*__F_XG_PUSH_H__*/

