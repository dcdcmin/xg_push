#include "xg_push_builder.h"
#include "f_md5.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "url_encode.h"


typedef struct _XGPushBuilder{
	char* access_id;
	char* secret_key;
}XGPushBuilder;

#define ANDROID_MESSAGE_TYPE	2
#define IOS_MESSAGE_TYPE  	    0

XGPushBuilder* xg_push_builder_create(char* access_id, char* secret_key){
	return_val_if_fail(access_id != NULL && secret_key != NULL, NULL);

	XGPushBuilder* thiz = (XGPushBuilder*)malloc(sizeof(XGPushBuilder));
	return_val_if_fail(thiz != NULL, NULL);

	thiz->access_id = strdup(access_id);
	thiz->secret_key = strdup(secret_key);

	return thiz;
}

void xg_push_builder_destroy(XGPushBuilder* thiz){
	if(thiz){
		if(thiz->access_id){
			free(thiz->access_id);
		}
		if(thiz->secret_key){
			free(thiz->secret_key);
		}
		free(thiz);
		thiz = NULL;
	}	
	
	return;
}

static int _get_md5_str(char* buff, char* md5_str, size_t max_len){
	MD5_CTX md5;  
	unsigned char decrypt[16];	
	
	f_md5_init(&md5);			 
	f_md5_update(&md5, buff, strlen((char *)buff));
	f_md5_final(&md5,decrypt);   

	int i = 0;
	for(i=0;i<16;i++)  {  
		sprintf(md5_str + i*2, "%02x", decrypt[i]);
	}

	return 0;
}

int xg_push_builder_build_android_notifation(XGPushBuilder* thiz, \
															char* title, \
															char* content, \
															char* token, \
															char* params, \
															size_t max_len){
	return_val_if_fail(thiz != NULL && title != NULL && content != NULL && token != NULL && params != NULL, -1);
	
	char buff[2048] = {0};
	unsigned int timestamp = time(NULL);
	int message_type = ANDROID_MESSAGE_TYPE;
	char message[256] = {0};

	sprintf(message, "{\"content\":\"%s\",\"title\":\"%s\"}", content, title);
    sprintf(buff,
		"POSTopenapi.xg.qq.com/v2/push/single_device"
		"access_id=%s"
		"device_token=%s"
		"message=%s"
		"message_type=%u"
		"timestamp=%u"
		"%s",		
		thiz->access_id, token, message, message_type, timestamp, thiz->secret_key);


	char md5_str_buff[64] = {0};
	_get_md5_str(buff, md5_str_buff, 64);
	printf("md5_str = %s \n\n", md5_str_buff);

	int url_encode_str_len = 0;
	char* url_encode_str = NULL;

	url_encode_str = url_encode(message, strlen(message), &url_encode_str_len);
	if(url_encode_str == NULL){
		printf("Error: url encode failed!\n");
		return -1;
	}

	sprintf(params,
		"access_id=%s"
		"&sign=%s"
		"&device_token=%s"
		"&message=%s"
		"&message_type=%u"
		"&timestamp=%u",
		thiz->access_id, md5_str_buff, token, url_encode_str, message_type, timestamp);

	free(url_encode_str);
	return 0;
}

int xg_push_builder_build_ios_notifation(XGPushBuilder* thiz, \
															XGPushEnvironment env,\
															char* content, \
															char* token, \
															char* params, \
															size_t max_len){
	return_val_if_fail(thiz != NULL && content != NULL && token != NULL && params != NULL, -1);
	
	char buff[2048] = {0};
	unsigned int timestamp = time(NULL);
	int message_type = IOS_MESSAGE_TYPE;
	char message[256] = {0};

	sprintf(message, "{\"aps\":{%s}}", content);
	
    sprintf(buff,
		"POSTopenapi.xg.qq.com/v2/push/single_device"
		"access_id=%s"
		"device_token=%s"
		"environment=%d"
		"message=%s"
		"message_type=%u"
		"timestamp=%u"
		"%s",		
		thiz->access_id, token, (int)env, message, message_type, timestamp, thiz->secret_key);


	char md5_str_buff[64] = {0};
	_get_md5_str(buff, md5_str_buff, 64);
	printf("md5_str = %s \n\n", md5_str_buff);

	int url_encode_str_len = 0;
	char* url_encode_str = NULL;

	url_encode_str = url_encode(message, strlen(message), &url_encode_str_len);
	if(url_encode_str == NULL){
		printf("Error: url encode failed!\n");
		return -1;
	}

	sprintf(params,
		"access_id=%s"
		"&sign=%s"
		"&device_token=%s"
		"&environment=%d"
		"&message=%s"
		"&message_type=%u"
		"&timestamp=%u",
		thiz->access_id, md5_str_buff, token, (int)env, url_encode_str, message_type, timestamp);

	free(url_encode_str);
	return 0;
}

