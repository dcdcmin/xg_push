#ifndef __F_HTTP_CLIENT_H__
#define __F_HTTP_CLIENT_H__


struct _FHttpClient;
typedef struct _FHttpClient FHttpClient;

FHttpClient* f_http_client_create(const char* server_ip, int port);
void f_http_client_destroy(FHttpClient* thiz);

int f_http_client_set_url(FHttpClient* thiz, char* url);
int f_http_client_set_host(FHttpClient* thiz, char* host);
int f_http_client_set_user_agent(FHttpClient* thiz, char* user_agent);
int f_http_client_set_content_type(FHttpClient* thiz, char* content_type);

int f_http_client_post(FHttpClient* thiz, char* post_data, size_t post_data_len);




#endif /*__F_HTTP_CLIENT_H__*/
