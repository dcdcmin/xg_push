#ifndef __XG_PUSH_BUILDER_H__
#define __XG_PUSH_BUILDER_H__

#include <stdio.h>
#include "f_xg_typedef.h"


struct _XGPushBuilder;
typedef struct _XGPushBuilder XGPushBuilder;

XGPushBuilder* xg_push_builder_create(char* access_id, char* secret_key);
void xg_push_builder_destroy(XGPushBuilder* thiz);
int xg_push_builder_build_android_notifation(XGPushBuilder* thiz, \
															char* title, \
															char* content, \
															char* token, \
															char* params, \
															size_t max_len);

int xg_push_builder_build_ios_notifation(XGPushBuilder* thiz, \
															XGPushEnvironment env,\
															char* content, \
															char* token, \
															char* params, \
															size_t max_len);

#endif /*__XG_PUSH_BUILDER_H__*/
