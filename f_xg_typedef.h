#ifndef __F_XG_TYPEDEF_H__
#define __F_XG_TYPEDEF_H__

#include <stdio.h>


#define return_val_if_fail(p, val) if(!(p))\
			{printf("%s:%d Warning: "#p" failed.\n",\
								__func__, __LINE__); return (val);}


#define return_if_fail(p) if(!(p))\
			{printf("%s:%d Warning: "#p" failed.\n", \
									__func__, __LINE__); return;}

typedef enum _XGPushEnvironment{
	XG_PUSH_ENV_NOT_DEFINE = 0,
	XG_PUSH_ENV_PRODUCT = 1,
	XG_PUSH_ENV_DEV = 2,
}XGPushEnvironment;


#endif /*__F_XG_TYPEDEF_H__*/

